package info.guardianproject.apthelper;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;

import org.littleshoot.proxy.HttpProxyServer;
import org.littleshoot.proxy.HttpProxyServerBootstrap;
import org.littleshoot.proxy.impl.DefaultHttpProxyServer;
import org.littleshoot.proxy.impl.ProxyToServerConnection;

import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.Properties;

import info.guardianproject.tfd.TcpForwardDaemon;
import info.pluggabletransports.dispatch.Connection;
import info.pluggabletransports.dispatch.DispatchConstants;
import info.pluggabletransports.dispatch.Dispatcher;
import info.pluggabletransports.dispatch.transports.legacy.Obfs4Transport;

public class AptHelper {

    public interface SetupCallback {
        void setupComplete(AptInfo aptInfo);
        void setupError(Exception e);
    }

    public static void setupApt(final Context context, final String bridgeline, final SetupCallback setupCallback) {


        //final AptHelper.HttpToSocksProxyConfig littleProxyConfig = new AptHelper.HttpToSocksProxyConfig("127.0.0.1", 8088);
        new Thread(){
            @Override
            public void run() {
                final AptInfo aptInfo = new AptInfo();

                try {
                    AptConfig config = AptHelper.initApt(context, bridgeline);

                    //TcpToSocksForwardDaemonConfig forwardDaemonConfig = new TcpToSocksForwardDaemonConfig("127.0.0.1", 9099);
                    InetSocketAddress forwardDaemonConfig = AptHelper.initTcpForwardDaemon(config.getPtClientConfig(), config.getPtBridgeConfig());

                    InetSocketAddress httpProxyConfig = AptHelper.initLittleProxy(forwardDaemonConfig);
                    aptInfo.setHttpProxyConfig(httpProxyConfig);
                } catch (Exception e) {
                    setupCallback.setupError(e);
                }

                if (setupCallback != null) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            setupCallback.setupComplete(aptInfo);
                        }
                    });
                }
            }
        }.start();
    }

    public static class AptConfig {
        //final HttpToSocksProxyConfig httpProxyConfig;
        //final TcpToSocksForwardDaemonConfig forwardDaemonConfig;
        final PtBridgeConfig ptBridgeConfig;
        final PtClientConfig ptClientConfig;

        public AptConfig(/*HttpToSocksProxyConfig httpProxyConfig, TcpToSocksForwardDaemonConfig forwardDaemonConfig,*/ PtClientConfig ptClientConfig, PtBridgeConfig ptBridgeConfig) {
            //this.httpProxyConfig = httpProxyConfig;
            //this.forwardDaemonConfig = forwardDaemonConfig;
            this.ptBridgeConfig = ptBridgeConfig;
            this.ptClientConfig = ptClientConfig;
        }

        public PtBridgeConfig getPtBridgeConfig() {
            return ptBridgeConfig;
        }

        public PtClientConfig getPtClientConfig() {
            return ptClientConfig;
        }
    }

    public static class HttpToSocksProxyConfig extends ProxyToServerConnection.Socks5Settings {
        public HttpToSocksProxyConfig(String host, int port){
            super(host, port);
        }
    };

    public static class TcpToSocksForwardDaemonConfig extends ProxyToServerConnection.Socks5Settings {
        public TcpToSocksForwardDaemonConfig(String host, int port) {
            super(host, port);
        }
    }

    public static class PtBridgeConfig extends ProxyToServerConnection.Socks5Settings {
        public PtBridgeConfig(String host, int port, String user, String pass) {
            super(host, port, user, pass);
        }
    }

    public static class PtClientConfig extends ProxyToServerConnection.Socks5Settings {
        public PtClientConfig(String host, int port) {
            super(host, port);
        }
    }

    public static InetSocketAddress initTcpForwardDaemon(PtClientConfig ptClientConfig, PtBridgeConfig ptBridgeConfig) {
        return initTcpForwardDaemon(null, ptClientConfig, ptBridgeConfig);
    }

    public static InetSocketAddress initTcpForwardDaemon(TcpToSocksForwardDaemonConfig forwardDaemonConfig, PtClientConfig ptClientConfig, PtBridgeConfig ptBridgeConfig) {

        final TcpForwardDaemon forwardDaemon = forwardDaemonConfig == null ?
                new TcpForwardDaemon(
                        ptBridgeConfig.socks5Ip,
                        ptBridgeConfig.socks5Port,
                        ptClientConfig.socks5Ip,
                        ptClientConfig.socks5Port,
                        ptBridgeConfig.socks5User,
                        ptBridgeConfig.socks5Pass) :
                new TcpForwardDaemon(
                        forwardDaemonConfig.socks5Ip,
                        forwardDaemonConfig.socks5Port,
                        ptBridgeConfig.socks5Ip,
                        ptBridgeConfig.socks5Port,
                        ptClientConfig.socks5Ip,
                        ptClientConfig.socks5Port,
                        ptBridgeConfig.socks5User,
                        ptBridgeConfig.socks5Pass);

        forwardDaemon.setErrorListener(new TcpForwardDaemon.ErrorListener() {
            @Override
            public void error(Throwable error) {
                Log.e("#pt", "tcp forward daemon encountered an error", error);
            }
        });

        new Thread(){
            @Override
            public void run() {
                forwardDaemon.run();
            }
        }.start();


        long timeout = System.currentTimeMillis() + 4000;
        while (forwardDaemon.getListeningAddress() == null || System.currentTimeMillis() < timeout) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
            }
        }
        if (forwardDaemon.getListeningAddress() != null)
        Log.i("#pt", "initialized tcp forward daemon: " + forwardDaemon.getListeningAddress().getHostName() + ":" + forwardDaemon.getListeningAddress().getPort());
        return forwardDaemon.getListeningAddress();
    }

    public static String parseBridgelineArgs(String bridgeline) {
        StringBuilder ret = new StringBuilder();
        String[] tokens = bridgeline.split(" ");
        for (int i=3; i<tokens.length; i++) {
            if (i != 3) {
                ret.append(";");
            }
            ret.append(tokens[i]);
        }
        return ret.toString();
    }

    public static String parseBridgelineHost(String bridgeline) {
        String[] tockes = bridgeline.split(" ");
        String[] addrTokens = tockes[1].split(":");
        return TextUtils.join(":", Arrays.copyOfRange(addrTokens, 0, addrTokens.length-1));
    }

    public static int parseBridgelinePort(String bridgeline) {
        String[] tockes = bridgeline.split(" ");
        String[] addrTokens = tockes[1].split(":");
        return Integer.parseInt(addrTokens[addrTokens.length-1]);
    }

    public static InetSocketAddress initLittleProxy(InetSocketAddress forwardDaemonConfig){
        TcpToSocksForwardDaemonConfig addr = new TcpToSocksForwardDaemonConfig(forwardDaemonConfig.getHostName(), forwardDaemonConfig.getPort());
        return initLittleProxy(null, addr);
    }

    public static InetSocketAddress initLittleProxy(InetSocketAddress httpProxyConfig, TcpToSocksForwardDaemonConfig forwardDaemonConfig){
        // configure little proxy socks
        ProxyToServerConnection.socks5Settings = forwardDaemonConfig;
        HttpProxyServerBootstrap b = DefaultHttpProxyServer.bootstrap();
        if (httpProxyConfig != null) {
            b = b.withAddress(httpProxyConfig);
        } else {
            b = b.withAddress(new InetSocketAddress("127.0.0.1", 0));
        }
        HttpProxyServer server = b.start();
        Log.i("#pt", "initialized littleproxy: " + server.getListenAddress().getHostName() + ":" + server.getListenAddress().getPort());
        return server.getListenAddress();
    }

    public static AptConfig initApt(Context context, String bridgeline){

        PtClientConfig ptClientConfig = null;
        PtBridgeConfig ptBridgeConfig = null;

        new Obfs4Transport().register();
        Properties options = new Properties();
        Obfs4Transport.setPropertiesFromBridgeString(options, bridgeline);
        Obfs4Transport transport = (Obfs4Transport) Dispatcher.get().getTransport(context, DispatchConstants.PT_TRANSPORTS_OBFS4, options);
        Connection connection = transport.connect(options.getProperty("address"));
        if (connection != null) {
            ptClientConfig = new PtClientConfig(connection.getLocalAddress().getHostAddress(), connection.getLocalPort());
            ptBridgeConfig = new PtBridgeConfig(connection.getRemoteAddress().getHostAddress(), connection.getRemotePort(), connection.getProxyUsername(), connection.getProxyPassword());
        }
        return new AptConfig(ptClientConfig, ptBridgeConfig);
    }
}
